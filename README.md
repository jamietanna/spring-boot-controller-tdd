# Creating a Spring Boot controller, using Test Driven Development

An example repo to go with [_Shift Your Testing Left with Spring Boot Controllers_](https://www.jvt.me/posts/2021/11/28/spring-boot-controller-tdd/).
